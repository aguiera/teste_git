package edu.up.banco.entidades;

public class Endereco {

	private String logradouro;
	private String numero;
	private String complemento;
	private String CEP;
	private String bairro;
	private String cidade;
	private String UF;
	
	public Endereco() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Endereco(String logradouro, String numero, String complemento, String cEP, String bairro, String cidade,
			String uF) {
		super();
		this.logradouro = logradouro;
		this.numero = numero;
		this.complemento = complemento;
		CEP = cEP;
		this.bairro = bairro;
		this.cidade = cidade;
		UF = uF;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCEP() {
		return CEP;
	}

	public void setCEP(String cEP) {
		CEP = cEP;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUF() {
		return UF;
	}

	public void setUF(String uF) {
		UF = uF;
	}

	@Override
	public String toString() {
		return "Endereco [logradouro=" + logradouro + ", numero=" + numero + ", complemento=" + complemento + ", CEP="
				+ CEP + ", bairro=" + bairro + ", cidade=" + cidade + ", UF=" + UF + "]";
	}
	
}
