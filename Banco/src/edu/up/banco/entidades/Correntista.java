package edu.up.banco.entidades;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Set;

public class Correntista {
	private String nome;
	private String sobrenome;
	private LocalDate dataNascimento;
	private HashMap<String, Endereco> enderecos = new HashMap<String, Endereco>();
	
	public Correntista() {
		super();
	}
	
	public Correntista(String nome, String sobrenome, LocalDate dataNascimento) {
		super();
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.dataNascimento = dataNascimento;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public void addEndereco(String qualificador, Endereco endereco) {
		this.enderecos.put(qualificador, endereco);
	}
	
	public Endereco getEndereco(String qualificador) {
		return this.enderecos.get(qualificador);
	}
	
	public void removeEndereco(String qualificador) {
		this.enderecos.remove(qualificador);
	}
	
	public Set<String> listaEnderecosCadastrados() {
		return this.enderecos.keySet();
	}

	@Override
	public String toString() {
		return "Correntista [nome=" + nome + ", sobrenome=" + sobrenome + ", dataNascimento=" + dataNascimento
				+ ", enderecos=" + enderecos + "]";
	}
	
	
}
