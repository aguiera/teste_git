package edu.up.banco;

import java.time.LocalDate;

import edu.up.banco.entidades.Correntista;
import edu.up.banco.entidades.Endereco;

public class Principal {

	public static void main(String[] args) {
		Correntista correntista = new Correntista("Anderson", "Guiera", LocalDate.of(1977, 11, 6));
		Endereco endereco = new Endereco("Pra�a Os�rio", "255A", "", "80125-325", "Centro", "Curitiba", "PR");
		
		correntista.addEndereco("Residencial", endereco);
		endereco = new Endereco("Rua Prof. Pedro V. P. Souza", "1055", "", "80358-500", "Cidade Industrial", "Curitiba", "PR");
		
		correntista.addEndereco("Comercial", endereco);
		
		System.out.println(correntista);
	}

}
